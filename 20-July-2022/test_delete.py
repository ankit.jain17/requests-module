import requests
api_url = "https://jsonplaceholder.typicode.com/todos/10"
response = requests.get(api_url)
print(response.json())
print(response.status_code)
response = requests.delete(api_url)
print(response.status_code)
r = response.json()
assert r == {}
