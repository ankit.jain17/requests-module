import json

import requests

url = "https://reqres.in/api/users"


file = open("CreateUser.json", "r")
json_input = file.read()
request_json = json.loads(json_input)

print(request_json)

response = requests.post(url,data = request_json)

print(response.content)

print(response.status_code)

assert response.status_code == 201

print(response.headers.get("Content-Length"))
print(response.text)
print(response.content)
print(response.headers)
response_json = json.loads(response.text)

print(response_json["job"])

